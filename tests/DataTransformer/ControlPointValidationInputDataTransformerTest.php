<?php

declare(strict_types=1);

namespace App\Tests\DataTransformer;

use App\DataTransformer\ControlPointValidationInputDataTransformer;
use App\Dto\ControlPointValidationInput;
use App\Entity\ControlPoint;
use App\Entity\ControlPointValidation;
use App\Entity\Task;
use App\Entity\User;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Security;

/**
 * Class test of ControlPointValidationInputDataTransformer
 */
class ControlPointValidationInputDataTransformerTest extends TestCase
{
    private Security $security;
    private ControlPointValidationInputDataTransformer $transformer;

    protected function setUp(): void
    {
        $this->security    = $this->createMock(Security::class);
        $this->transformer = new ControlPointValidationInputDataTransformer($this->security);
    }

    /**
     * Test supports transformation expecting false
     */
    public function testDoesNotSupportTransformation(): void
    {
        $controlPoint = new ControlPoint(new Task());
        $actual       = $this->transformer->supportsTransformation(new ControlPointValidation($controlPoint), '', []);

        $this->assertFalse($actual);
    }

    /**
     * Test supports transformation expecting true
     */
    public function testSupportsTransformation(): void
    {
        $controlPoint = new ControlPoint(new Task());
        $context      = [
            'input'                     => ['class' => ControlPointValidationInput::class],
            'collection_operation_name' => 'post',
        ];
        $actual       = $this->transformer->supportsTransformation(['controlPoint' => $controlPoint], ControlPointValidation::class, $context);

        $this->assertTrue($actual);
    }

    /**
     * Test transformation with anonymous user, expecting exception
     */
    public function testTransformException(): void
    {
        $this->expectException(AccessDeniedException::class);
        $this->security
            ->expects($this->once())
            ->method('getUser')
            ->willReturn(null);

        $this->transformer->transform(new ControlPointValidationInput(), ControlPointValidation::class, []);
    }

    /**
     * Test transformation expecting success
     */
    public function testTransform(): void
    {
        $this->security
            ->expects($this->once())
            ->method('getUser')
            ->willReturn(new User('foo-guid'));

        $controlPoint        = new ControlPoint(new Task());
        $input               = new ControlPointValidationInput();
        $input->controlPoint = $controlPoint;

        $expected = (new ControlPointValidation($controlPoint))->setVerifier('foo-guid');
        $actual   = $this->transformer->transform($input, ControlPointValidation::class, []);

        $this->assertEqualsWithDelta($expected, $actual, 1);
    }
}
