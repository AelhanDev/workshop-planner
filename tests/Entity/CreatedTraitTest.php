<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use PHPUnit\Framework\TestCase;

/**
 * Test class of CreatedTrait
 */
class CreatedTraitTest extends TestCase
{

    public function testCreatedAt(): void
    {
        $entity    = new CreatedAtEntity();
        $createdAt = $entity->getCreatedAt();
        $this->assertInstanceOf(\DateTimeImmutable::class, $createdAt);

        $entity->resetCreatedAt();

        $this->assertNotEquals($createdAt, $entity->getCreatedAt());
    }
}
