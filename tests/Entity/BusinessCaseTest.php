<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\BusinessCase;
use App\Entity\Task;
use PHPUnit\Framework\TestCase;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntityValidator;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class BusinessCaseTest
 */
class BusinessCaseTest extends TestCase
{
    /**
     * Test validation on entity expecting failure
     */
    public function testValidationFailed(): void
    {
        $tooLongString = str_repeat('foo', 86);
        $case = new BusinessCase();
        $case->setCode($tooLongString)
            ->setDueDate(new \DateTime('-1 day'))
            ->setName($tooLongString);

        $violations = $this->getValidator()->validate($case);

        $this->assertCount(3, $violations);
    }

    /**
     * Test validation on entity expecting success
     */
    public function testValidationSucceed(): void
    {
        $case = new BusinessCase();
        $case->setCode('foo')
            ->setDueDate(new \DateTime('+10 day'))
            ->setName('bar');

        $violations = $this->getValidator()->validate($case);

        $this->assertCount(0, $violations);
    }

    /**
     * Test clone behavior
     */
    public function testClone(): void
    {
        $case = new BusinessCase();
        $case->setDueDate(new \DateTime('+1 day'))
            ->setName('Name')
            ->setDescription('Description')
            ->setCode('666')
            ->getTasks()->add(new Task());

        $copy = clone $case;

        // Expect ID, code & dates to be reset
        $this->assertNull($copy->getId());
        $this->assertNull($copy->getDueDate());
        $this->assertNull($copy->getCode());
        $this->assertNotEquals($case->getCreatedAt(), $copy->getCreatedAt());

        // Expected tasks to be cloned too
        $this->assertEquals($case->getTasks()->count(), $copy->getTasks()->count());
        $this->assertNotEquals($case->getTasks(), $copy->getTasks());

        // Expect the rest to be the same
        $this->assertEquals($case->getName(), $copy->getName());
        $this->assertEquals($case->getDescription(), $copy->getDescription());
    }

    /**
     * Get an instance of ValidatorInterface width disabled 'doctrine.orm.validator.unique'
     *
     * @return ValidatorInterface
     */
    protected function getValidator(): ValidatorInterface
    {
        $uniqueValidator = $this->createStub(UniqueEntityValidator::class);

        $validatorFactory = new ConstraintValidatorFactory();
        $validatorFactory->setValidator('doctrine.orm.validator.unique', $uniqueValidator);

        return Validation::createValidatorBuilder()
            ->setConstraintValidatorFactory($validatorFactory)
            ->enableAnnotationMapping(true)
            ->addDefaultDoctrineAnnotationReader()
            ->getValidator();
    }
}
