<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\CreatedTrait;

/**
 * A dummy class using CreatedTrait
 */
class CreatedAtEntity
{
    use CreatedTrait;
}
