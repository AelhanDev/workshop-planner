<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Entity\BusinessCase;
use App\Repository\BusinessCaseRepository;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;

/**
 * Class BusinessCaseTest
 *
 * @group functional
 */
class BusinessCaseTest extends AbstractApiTestCase
{
    use RefreshDatabaseTrait;

    public function testGetCollection(): void
    {
        $response = $this->createAuthenticatedClient()->request('GET', '/business_cases');

        $this->assertResponseIsSuccessful();
        $this->assertEquals(10, $response->toArray()['hydra:totalItems']);
    }

    public function testCreateBusinessCase(): void
    {
        $response = $this->createAuthenticatedClient()->request('POST', '/business_cases', [
            'headers' => [
                'Accept'       => 'application/json',
                'Content-Type' => 'application/json',
            ],
            'json'    => [
                'code'    => 'BC0228',
                'name'    => 'Rafale alu 7021',
                'dueDate' => (new \DateTime('+3 months'))->format('Y-m-d H:i:s'),
            ],
        ]);

        $this->assertResponseIsSuccessful();
        $response = $response->toArray();

        $this->assertArraySubset([
            'code' => 'BC0228',
            'name' => 'Rafale alu 7021',
        ], $response);
    }

    /**
     * Smoke test clone-business-case endpoint
     */
    public function testClone(): void
    {
        /** @var BusinessCase $businessCase */
        $businessCase = static::getContainer()->get(BusinessCaseRepository::class)->findOneBy([]);
        $client       = $this->createAuthenticatedClient();

        $client->request('POST', sprintf('/business_cases/%s/clone', $businessCase->getId()), [
            'json' => [
                'code' => 'H666',
                'name' => 'Hell case',
            ],
        ]);

        $this->assertResponseStatusCodeSame(201);
    }
}
