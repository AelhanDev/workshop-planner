<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Entity\ControlPointValidation;
use App\Repository\ControlPointRepository;
use App\Repository\ControlPointValidationRepository;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;

/**
 * @group functional
 */
class ControlPointValidationTest extends AbstractApiTestCase
{
    use RefreshDatabaseTrait;

    /**
     * Smoke test ControlPointValidation creation expecting success
     */
    public function testApproveControlPoint(): void
    {
        $controlPoint = static::getContainer()->get(ControlPointRepository::class)->findOneBy([]);

        $this->createAuthenticatedClient()->request('POST', '/control_point_validations', [
            'json' => [
                'controlPoint' => '/control_points/'.$controlPoint->getId(),
            ],
        ]);

        $this->assertResponseIsSuccessful();
    }

    /**
     * Smoke test removal of a control point validation, expecting success
     */
    public function testRemoveApproval(): void
    {
        /** @var ControlPointValidation $validation */
        $validation = static::getContainer()
            ->get(ControlPointValidationRepository::class)
            ->findOneBy(['verifier' => '0322b1fa-d41b-4a86-9d66-ff3db220c701']);

        $this->createAuthenticatedClient()->request('DELETE', '/control_point_validations/'.$validation->getId());

        $this->assertResponseIsSuccessful();
    }

    /**
     * Smoke test removal of someone else control point validation, expecting access denied
     */
    public function testCantRemoveApproval(): void
    {
        /** @var ControlPointValidation $validation */
        $validation = static::getContainer()
            ->get(ControlPointValidationRepository::class)
            ->findOneBy(['verifier' => '0322b1fa-d41b-4a86-9d66-ff3db220c701']);

        // See fixtures/users.yaml for matching guid
        $this->createAuthenticatedClient('0322b1fa-d41b-4a86-9d66-ff3db220c702')
            ->request('DELETE', '/control_point_validations/'.$validation->getId());

        $this->assertResponseStatusCodeSame(403);
    }
}
