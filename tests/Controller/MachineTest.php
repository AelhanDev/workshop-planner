<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;
use App\Repository\MachineRepository;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;

/**
 * Class MachineTest
 *
 * @group functional
 */
class MachineTest extends AbstractApiTestCase
{
    use RefreshDatabaseTrait;

    protected Client $client;

    /**
     * Name property of machines to be inserted in db for the following test cases
     */
    protected string $name = "Time-travelling Machine";

    /**
     * Sets up authenticated user
     */
    public function setUp() : void
    {
        $this->client = $this->createAuthenticatedClient();
    }

    /**
     * Assert a collection of machines is retrievable
     */
    public function testGetMachineCollection()
    {
        $response = $this->client->request('GET', '/machines');
        $this->assertResponseIsSuccessful('GET /machines does not return favorable result');
        $elements = $response->toArray()['hydra:totalItems'];

        $this->assertEquals($elements, 5, "GET /machines does not return expected number of elements");
    }

    /**
     * Assert a specific machine is retrievable
     *
     * @depends testGetMachineCollection
     */
    public function testGetOneMachine()
    {
        // Get the ID of the machine to be retrieved
        $response = $this->client->request('GET', '/machines');
        $machineId = array_column($response->toArray()['hydra:member'], 'id')[0];

        $this->client->request('GET', "/machines/$machineId");
        $this->assertResponseIsSuccessful('A specific machine is not retrievable');
    }

    /**
     * Assert machine collection is retrievable as a workforce collection
     */
    public function testGetMachineCollectionAsWorkforceCollection()
    {
        $this->client->request('GET', '/workforces');
        $this->assertResponseIsSuccessful('GET /workforces does not return favorable result');
    }

    /**
     * Assert a machine is retrievable as a workforce
     *
     * @depends testGetMachineCollectionAsWorkforceCollection
     */
    public function testGetMachineAsWorkforce()
    {
        // Get the ID of the machine to be retrieved
        $response = $this->client->request('GET', '/machines');
        $machineId = array_column($response->toArray()['hydra:member'], 'id')[0];

        $this->client->request('GET', "/workforces/$machineId");
        $this->assertResponseIsSuccessful('A specific machine is not retrievable as a workforce');
    }

    /**
     * Assert that a machine is creatable
     *
     * @depends testGetOneMachine
     */
    public function testCreateMachine()
    {
        // Assert a machine is creatable
        $response = $this->client->request('POST', '/machines', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [
                'name' => "$this->name",
            ],
        ]);
        $this->assertResponseStatusCodeSame(201, 'It is not possible to create a machine');
    }

    /**
     * Assert that a machine cannot be created
     * with a taken name
     *
     * @depends testCreateMachine
     */
    public function testCreateMachineWithTakenName()
    {
        // A taken machine name
        $response = $this->client->request('GET', '/machines');
        $takenName = array_column($response->toArray()['hydra:member'], 'name')[0];

        // Assert two machines cannot have the same name
        $this->client->request('POST', '/machines', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [
                'name' => "$takenName",
            ],
        ]);
        $this->assertResponseStatusCodeSame(422, 'A machine can be created with a taken name');
    }

    /**
     * Assert that a machine cannot be created
     * with a blank name
     *
     * @depends testCreateMachine
     */
    public function testCreateMachineWithBlankName()
    {
        // Assert a machine cannot have blank name
        $this->client->request('POST', '/machines', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [
                'name' => '',
            ],
        ]);
        $this->assertResponseStatusCodeSame(422, 'A machine can be created with a blank name');
    }
}
