<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;
use App\Repository\TeamRepository;

/**
 * Class TeamTest
 *
 * @group functional
 */
class TeamTest extends AbstractApiTestCase
{
    use RefreshDatabaseTrait;

    protected Client $client;

    /**
     * @var string Name property of team to be inserted in db for the following test cases
     */
    protected string $name = "Production Team";

    /**
     * Sets up authenticated user
     */
    public function setUp(): void
    {
        $this->client = $this->createAuthenticatedClient();
    }

    /**
     * Assert a collection of teams is retrievable
     */
    public function testGetTeamCollection()
    {
        $response = $this->client->request('GET', '/teams');
        $this->assertResponseIsSuccessful('GET /teams does not return favorable result');
        $elements = $response->toArray()['hydra:totalItems'];

        $this->assertEquals($elements, 3, "GET /teams does not return expected number of elements");
    }

    /**
     * Assert a specific team is retrievable
     *
     * @depends testGetTeamCollection
     */
    public function testGetOneTeam()
    {
        // Get the ID of the team to be retrieved
        $teamId = static::getContainer()->get(TeamRepository::class)->findOneByName('Administrative Team')->getId();

        $this->client->request('GET', "/teams/$teamId");
        $this->assertResponseIsSuccessful('A specific team is not retrievable');
    }

    /**
     * Assert team collection is retrievable as a workforce collection
     */
    public function testGetTeamCollectionAsWorkforceCollection()
    {
        $response = $this->client->request('GET', '/workforces');
        $this->assertResponseIsSuccessful('GET /workforces does not return favorable result');

        $elements = $response->toArray()['hydra:totalItems'];

        $this->assertEquals(8, $elements, "GET /workforces does not return the expected number of elements");
    }

    /**
     * Assert a team is retrievable as a workforce
     */
    public function testGetTeamAsWorkforce()
    {
        // Get the ID of the team to be retrieved
        $teamId = static::getContainer()->get(TeamRepository::class)->findOneByName('Administrative Team')->getId();

        $this->client->request('GET', "/workforces/$teamId");
        $this->assertResponseIsSuccessful('A specific team is not retrievable as a workforce');
    }

    /**
     * Assert that a team is creatable, and that created team is retrievable
     *
     * @depends testGetOneTeam
     */
    public function testCreateTeam()
    {
        // Assert a team is creatable
        $response = $this->client->request('POST', '/teams', [
            'headers' => ['Content-Type' => 'application/json'],
            'json'    => [
                'name' => "$this->name",
            ],
        ]);
        $this->assertResponseIsSuccessful();
        $teamName = $response->toArray()['name'];
        $this->assertEquals($this->name, $teamName, 'Name property of a team is not in the expected state after creation');
    }

    /**
     * Assert that a machine cannot be created with a blank name
     *
     * @depends testCreateTeam
     */
    public function testCreateTeamWithBlankName()
    {
        // Assert a machine cannot have blank name
        $this->client->request('POST', '/teams', [
            'headers' => ['Content-Type' => 'application/json'],
            'json'    => [
                'name' => '',
            ],
        ]);
        $this->assertResponseStatusCodeSame(422, 'A team can be created with a blank name');
    }

    /**
     * Assert that a team can be patched and that the patched team is in the expected state
     */
    public function testPatchTeam()
    {
        // Get the ID of the team to be patched
        $teamId = static::getContainer()->get(TeamRepository::class)->findOneByName('Administrative Team')->getId();

        // Assert a team can be patched
        $response = $this->client->request('PATCH', "/teams/$teamId", [
            'headers' => ['Content-Type' => 'application/merge-patch+json'],
            'json'    => [
                'name' => "$this->name",
            ],
        ]);
        $this->assertResponseIsSuccessful('A team is not updatable using PATCH');

        // Assert the patched team is in the expected state
        $teamName   = $response->toArray()['name'];
        $this->assertEquals($this->name, $teamName, 'PATCH /teams : updated name is not in the expected state');
    }

    /**
     * Assert users can be added to a team
     *
     * @depends testPatchTeam
     */
    public function testUsersCanBeAddedToTeam()
    {
        // Get two users IRIs to add them to a team
        $response      = $this->client->request('GET', '/users');
        $firstUserIri  = array_column($response->toArray()['hydra:member'], '@id')[0];
        $secondUserIri = array_column($response->toArray()['hydra:member'], '@id')[1];

        $response = $this->client->request('POST', "/teams", [
            'headers' => ['Content-Type' => 'application/json'],
            'json'    => [
                'name'  => 'A team that contains two different users',
                'users' => [
                    sprintf('%s', $firstUserIri),
                    sprintf('%s', $secondUserIri),
                ],
            ],
        ]);
        $this->assertResponseIsSuccessful('Several users cannot be added to a team');

        $users = $response->toArray()['users'];
        $this->assertEquals(sizeof($users), 2, 'It is possible to create a team with duplicate user in it');
    }

    /**
     * Assert a team cannot be created with duplicate user in it
     *
     * @depends testCreateTeam
     */
    public function testCreateTeamWithDuplicateUser()
    {
        $response = $this->client->request('GET', '/users');
        $userIri  = array_column($response->toArray()['hydra:member'], '@id')[0];

        $response = $this->client->request('POST', '/teams', [
            'headers' => ['Content-Type' => 'application/json'],
            'json'    => [
                'name'  => 'This team should contain one User',
                'users' => [
                    "$userIri",
                    "$userIri",
                ],
            ],
        ]);
        $this->assertResponseStatusCodeSame(201);

        // Counting the users to assert there is no duplicate
        $users = $response->toArray()['users'];
        $this->assertEquals(sizeof($users), 1, 'It is possible to create a team with duplicate user in it');
    }
}
