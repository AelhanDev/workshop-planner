<?php

declare(strict_types=1);

namespace App\Tests\Security\Voter;

use App\Entity\ControlPoint;
use App\Entity\ControlPointValidation;
use App\Entity\Task;
use App\Entity\User;
use App\Security\Voter\ControlPointValidationVoter;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManager;

/**
 * Test class of ControlPointValidationVoter
 */
class ControlPointValidationVoterTest extends TestCase
{
    private ControlPointValidation $subject;

    private AccessDecisionManager $manager;

    protected function setUp(): void
    {
        $verifier      = '/auth/users/1ec73aa5-600d-6176-81b6-35b8f7a8c675';
        $task          = new Task();
        $controlPoint  = new ControlPoint($task);
        $this->subject = (new ControlPointValidation($controlPoint))->setVerifier($verifier);
        $this->manager = new AccessDecisionManager([new ControlPointValidationVoter()]);
    }

    public function testCanDelete(): void
    {
        $user         = new User('/auth/users/1ec73aa5-600d-6176-81b6-35b8f7a8c675');
        $token        = $this->createMock(TokenInterface::class);
        $token->expects($this->once())
            ->method('getUser')
            ->willReturn($user);
        $actual = $this->manager->decide($token, ['DELETE'], $this->subject);

        $this->assertTrue($actual);
    }

    public function testAnonymousCantDelete(): void
    {
        $token        = $this->createMock(TokenInterface::class);
        $token->expects($this->once())
            ->method('getUser')
            ->willReturn(null);
        $actual = $this->manager->decide($token, ['DELETE'], $this->subject);

        $this->assertFalse($actual);
    }

    public function testCantDelete(): void
    {
        $user         = new User('/auth/users/different-uuid');
        $token        = $this->createMock(TokenInterface::class);
        $token->expects($this->once())
            ->method('getUser')
            ->willReturn($user);
        $actual = $this->manager->decide($token, ['DELETE'], $this->subject);

        $this->assertFalse($actual);
    }
}
