<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\BusinessCase;
use App\Repository\BusinessCaseRepository;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Make a copy of a business case
 */
#[AsController]
class CloneBusinessCaseController
{
    public function __construct(private BusinessCaseRepository $repository)
    {
    }

    public function __invoke(string $id, BusinessCase $data)
    {
        $model = $this->repository->findDeep($id);

        if ($model) {
            $copy = clone $model;
            $copy
                ->setCode($data->getCode())
                ->setName($data->getName());

            return $copy;
        }

        throw new NotFoundHttpException("Resource $id not found");
    }
}
