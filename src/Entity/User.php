<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Lexik\Bundle\JWTAuthenticationBundle\Security\User\JWTUserInterface;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;

/**
 * Class User
 */
#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: "`user`")]
#[ApiResource(
    collectionOperations: ['get'],
    itemOperations: ['get'],
)]
class User implements JWTUserInterface
{

    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    private ?string $id;

    /**
     * Global identifier
     */
    #[ORM\Column(type: 'string', length: 255, unique: true)]
    private string $guid;

    #[ORM\Column(type: 'json')]
    private array $roles = [];

    public function __construct(string $guid)
    {
        $this->guid = $guid;
    }

    public static function createFromPayload($username, array $payload): self
    {
        return new self($username);
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getGuid(): string
    {
        return $this->guid;
    }

    public function getUserIdentifier(): string
    {
        return $this->guid;
    }

    public function getUsername(): string
    {
        return $this->guid;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @codeCoverageIgnore
     *
     * @return string|null
     */
    #[ApiProperty(readable: false)]
    public function getPassword(): ?string
    {
        return null;
    }

    /**
     * @codeCoverageIgnore
     *
     * @return string|null
     */
    #[ApiProperty(readable: false)]
    public function getSalt(): ?string
    {
        return null;
    }

    public function eraseCredentials()
    {
        // Nothing to do. Leave blank
    }
}
