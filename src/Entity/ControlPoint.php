<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\ControlPointRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Control point resource
 */
#[ORM\Entity(repositoryClass: ControlPointRepository::class)]
#[ApiResource]
#[ApiFilter(SearchFilter::class, properties: ['task' => 'exact'])]
class ControlPoint
{
    use CreatedTrait;

    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    private ?string $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Length(max: 255)]
    #[Assert\NotBlank]
    private string $description = '';

    #[ORM\ManyToOne(targetEntity: Task::class, inversedBy: 'controlPoints')]
    #[ORM\JoinColumn(nullable: false)]
    private Task $task;

    /**
     * List of user that validated the control point
     *
     * @var Collection<ControlPointValidation>
     */
    #[ORM\OneToMany(mappedBy: 'controlPoint', targetEntity: ControlPointValidation::class, cascade: ['remove'], orphanRemoval: true)]
    #[ApiProperty(writable: false)]
    private Collection $controlPointValidations;

    public function __construct(Task $task)
    {
        $this->task                    = $task;
        $this->controlPointValidations = new ArrayCollection();
        $this->resetCreatedAt();
    }

    public function __clone()
    {
        $this->resetCreatedAt();
        // Reset all validations
        $this->controlPointValidations = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getTask(): Task
    {
        return $this->task;
    }

    public function setTask(Task $task): self
    {
        $this->task = $task;

        return $this;
    }

    /**
     * @return Collection|ControlPointValidation[]
     */
    public function getControlPointValidations(): Collection
    {
        return $this->controlPointValidations;
    }

    public function addControlPointValidation(ControlPointValidation $controlPointValidation): self
    {
        if (!$this->controlPointValidations->contains($controlPointValidation)) {
            $this->controlPointValidations[] = $controlPointValidation;
            $controlPointValidation->setControlPoint($this);
        }

        return $this;
    }
}
