<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\ControlPointValidation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ControlPointValidation|null find($id, $lockMode = null, $lockVersion = null)
 * @method ControlPointValidation|null findOneBy(array $criteria, array $orderBy = null)
 * @method ControlPointValidation[]    findAll()
 * @method ControlPointValidation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ControlPointValidationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ControlPointValidation::class);
    }
}
