<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add color to business_case
 */
final class Version20220125175930 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add color to business_case';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE business_case ADD color VARCHAR(9) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE business_case DROP color');
    }
}
