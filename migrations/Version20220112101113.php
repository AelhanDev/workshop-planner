<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220112101113 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE control_point (id UUID NOT NULL, task_id UUID NOT NULL, description VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_C5C5A798DB60186 ON control_point (task_id)');
        $this->addSql('COMMENT ON COLUMN control_point.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN control_point.task_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN control_point.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE control_point_validation (id UUID NOT NULL, control_point_id UUID NOT NULL, verifier VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_9D7776CC1FE83EE2 ON control_point_validation (control_point_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9D7776CC1FE83EE2FB914A48 ON control_point_validation (control_point_id, verifier)');
        $this->addSql('COMMENT ON COLUMN control_point_validation.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN control_point_validation.control_point_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN control_point_validation.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE control_point ADD CONSTRAINT FK_C5C5A798DB60186 FOREIGN KEY (task_id) REFERENCES task (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE control_point_validation ADD CONSTRAINT FK_9D7776CC1FE83EE2 FOREIGN KEY (control_point_id) REFERENCES control_point (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE control_point_validation DROP CONSTRAINT FK_9D7776CC1FE83EE2');
        $this->addSql('DROP TABLE control_point');
        $this->addSql('DROP TABLE control_point_validation');
    }
}
