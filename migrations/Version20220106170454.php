<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Mark all IDs as DC2Type:uuid
 */
final class Version20220106170454 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Mark all IDs as DC2Type:uuid';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('COMMENT ON COLUMN business_case.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN task.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN task.business_case_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN task.assigned_to_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN "user".id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN workforce.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN team_user.team_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN team_user.user_id IS \'(DC2Type:uuid)\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('COMMENT ON COLUMN workforce.id IS NULL');
        $this->addSql('COMMENT ON COLUMN team_user.team_id IS NULL');
        $this->addSql('COMMENT ON COLUMN team_user.user_id IS NULL');
        $this->addSql('COMMENT ON COLUMN "user".id IS NULL');
        $this->addSql('COMMENT ON COLUMN business_case.id IS NULL');
        $this->addSql('COMMENT ON COLUMN task.id IS NULL');
        $this->addSql('COMMENT ON COLUMN task.business_case_id IS NULL');
        $this->addSql('COMMENT ON COLUMN task.assigned_to_id IS NULL');
    }
}
