# Workshop Planner

Access full documentation :

* [Stable online documentation](https://savadenn-public.gitlab.io/workshop-tools/workshop-planner/docs/)
* [Documentation source](./docs)
* Build `dist/docs/WorkshopPlanner.pdf` locally
  ```
  make build.doc
  ```

## Contact

You may join us on our public [Matrix room](https://matrix.to/#/#workshop-tools:savadenn.ems.host)

## Authors and contributors

Workshop Planner is made by [Savadenn](https://www.savadenn.bzh) with the help of individual contributors.

The complete list can be found [here](https://gitlab.com/savadenn-public/workshop-tools/workshop-planner/-/graphs/stable).
