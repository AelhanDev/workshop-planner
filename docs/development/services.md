# Services

* API 
    * [Endpoint](https://workshop.docker.localhost/api)
    * [Swagger UI](https://workshop.docker.localhost/api/docs)
* [Documentation](https://docapi-workshop.docker.localhost)
